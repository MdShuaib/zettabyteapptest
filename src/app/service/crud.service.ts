import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CrudService {
  public url = environment.API_ENDPOINT;
  constructor(private http: HttpClient) { }
  public get(): Observable<any> {
    const url = `${this.url}${'users'}`;
    return this.http.get(url).map((res) => res)
      .do((data: Response) => {
        if (!data['IsSuccess']) {
          this.handleResponseError(data);
        }
      });
  }

  public getUserById(id: number): Observable<any> {
    const url = `${this.url}${'users'}/${id}/albums`;
    return this.http.get(url).map((res) => res)
      .do((data: Response) => {
        if (!data['IsSuccess']) {
          this.handleResponseError(data);
        }
      });
  }

  public getAlbumId(id: string): Observable<any> {
    const url = `${this.url}${'albums'}/${id}/${'photos'}`;
    return this.http.get(url).map((res) => res)
      .do((data: Response) => {
        if (!data['IsSuccess']) {
          this.handleResponseError(data);
        }
      });
  }
  private handleResponseError(data: any) {
    if (data && data.Error) {
      console.log('error occured');
    }
  }
}
