import { each } from 'lodash';
import { Subscription } from 'rxjs/Subscription';

export class UtilityFunctions {
    unSubscribeSubscriptions(subscriptions: Subscription[]) {
        each(subscriptions, (data) => {
            data.unsubscribe();
        });
    }


}


