// core imports
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';

// application imports
import { CrudService } from '../service/crud.service';
import { Subscription } from 'rxjs/Subscription';
import { UtilityFunctions } from '../service/utility-function';
import { each } from 'lodash';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private utilityFunctions = new UtilityFunctions();
  urlId: string;
  photoDetails: any = [];
  constructor(
      private crudService: CrudService,
      private router: Router,
      private _location: Location) { }

  ngOnInit() {
    this.urlId = this.router.url.match(/\d+/)[0];
    this.getPhotos(this.urlId);
  }

  getPhotos(id: string) {
    this.subscriptions.push(this.crudService.getAlbumId(id).subscribe((result: any) => {
      if (result) {
        each(result, (val) => {
          if (val.albumId === parseInt(id, 10)) {
            if (this.photoDetails.length <= 4) {
              this.photoDetails.push(val);
            }
          }
        });
      }
    }));
  }
  onBackView() {
    this._location.back();
  }
  ngOnDestroy() {
    this.utilityFunctions.unSubscribeSubscriptions(this.subscriptions);
  }
}
