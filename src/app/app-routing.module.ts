import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { AlbumComponent } from './album/album.component';
import { ViewUserComponent } from './user/view-user/view-user.component';
const routes: Routes = [
  { path: '', component: UserComponent},
  { path: 'user/:id/albums', component: ViewUserComponent },
  { path: 'albums/:id/photos', component: AlbumComponent},
  { path: '**', component: UserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
