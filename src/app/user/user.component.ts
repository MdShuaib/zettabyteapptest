// core imports
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// application imports
import { CrudService } from '../service/crud.service';
import { User } from './user.index';

// 3rd party import
import { Subscription } from 'rxjs/Subscription';
import { UtilityFunctions } from '../service/utility-function';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private utilityFunctions = new UtilityFunctions();
  userList: User[];
  selectedUser: User;
  constructor(
      private crudService: CrudService,
      private router: Router) { }

  ngOnInit() {
    this.getUserList();
  }

  public getUserList() {
    this.subscriptions.push(this.crudService.get().subscribe((result: any) => {
      if (result) {
        this.userList = result;
      }
    }));
  }
  onSelect(user: User): void {
    this.selectedUser = user;
  }
  ngOnDestroy() {
    this.utilityFunctions.unSubscribeSubscriptions(this.subscriptions);
  }

}
