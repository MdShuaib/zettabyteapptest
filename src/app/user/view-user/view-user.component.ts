// core imports
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';

// 3rd party imports
import { Subscription } from 'rxjs/Subscription';
import { UtilityFunctions } from 'src/app/service/utility-function';
import { each } from 'lodash';

// application imports
import { CrudService } from 'src/app/service/crud.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private utilityFunctions = new UtilityFunctions();
  userDetailsList: any = [];
  urlId: string;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private _location: Location) { }

  ngOnInit() {
    this.urlId = this.router.url.match(/\d+/)[0];
    this.getViewDetails(this.urlId);
  }
  getViewDetails(id) {
    this.subscriptions.push(this.crudService.getUserById(id).subscribe((result: any) => {
      if (result) {
        each(result, (val) => {
          if (val.userId === parseInt(id, 10)) {
            this.userDetailsList.push(val);
          }
        });
      }
    }));
  }

  onBackView() {
    this._location.back();
  }
  ngOnDestroy() {
    this.utilityFunctions.unSubscribeSubscriptions(this.subscriptions);
  }

}
